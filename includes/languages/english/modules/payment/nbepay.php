<?php
/**
 * MOLPay osCommerce Plugin
 * 
 * @package Payment Gateway
 * @author MOLPay Technical Team <technical@molpay.com>
 * @version 2.0.0
 */

define('MODULE_PAYMENT_nbepay_TEXT_TITLE', 'NBePay Online Payment Gateway (Visa, MasterCard, Maybank2u, MEPS, FPX, etc)');
define('MODULE_PAYMENT_nbepay_TEXT_DESCRIPTION', 'NetBuilder NBePay Online Payment Gateway is a service to help Malaysian merchant to sell online. <a href="http://www.netbuilder.com.my" target=_blank>http://www.netbuilder.com.my</a>');
define('MODULE_PAYMENT_nbepay_TEXT_TYPE', 'Type:');
define('MODULE_PAYMENT_nbepay_TEXT_CREDIT_CARD_NUMBER', 'Credit Card Number:');
define('MODULE_PAYMENT_nbepay_TEXT_CREDIT_CARD_OWNER', 'Credit Card Name:');
define('MODULE_PAYMENT_nbepay_TEXT_CREDIT_CARD_TYPE', 'Credit Card Type:');
define('MODULE_PAYMENT_nbepay_TEXT_CREDIT_CARD_EXPIRES', 'Credit Card Expiry Date:');
define('MODULE_PAYMENT_nbepay_TEXT_CREDIT_CARD_V2', 'Credit Card Security Check Code:');
define('MODULE_PAYMENT_nbepay_TEXT_CREDIT_CARD_V2_LOCATION', '(located on the back of the credit card)');
define('MODULE_PAYMENT_nbepay_TEXT_JS_CC_NUMBER', '* The credit card number must be at least ' . CC_NUMBER_MIN_LENGTH . ' characters.\n');
define('MODULE_PAYMENT_nbepay_TEXT_ERROR_MESSAGE', 'There has been an error processing your credit card, please try again.');
define('MODULE_PAYMENT_nbepay_TEXT_ERROR', 'Credit Card Error!');
?>
